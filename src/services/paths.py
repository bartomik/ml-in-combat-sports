"""
Describes the paths of data files. Implemented for usage in src/run/*
"""
import os
current_file = os.path.abspath(os.path.dirname(__file__))

PER_MIN = os.path.join(current_file, '../../data/features/avgs_per_minute/per_min.csv')

PER_MIN_WEIGHTED = os.path.join(current_file,
                                '../../data/features/avgs_per_minute/per_min_weighted.csv')
