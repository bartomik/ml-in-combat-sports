# Machine Learning in Combat Sports

This is a repository of the implementation of a Bachelor thesis on the topic of machine learning in combat sports. This work aims to maximize profit on the MMA betting market with a focus  on  long-term  robustness. This  includes  finding  a  predictive  model  and implementing a betting strategy.

The full thesis can be accessed at: 

The models analysed in the Chapter Results are saved in the directory final_models.

## How to use
The used Conda environment can be created using the environment.yml file from the root directory.

Make sure to first run from the root directory `pip install -e .` to install the project on your machine.

Runnable files (to be found in src/run):
1. train_model.py
    - trains and saves a pytorch model with given parameters/hyper parameters
   
2. evaluate_model.py
    - evaluates predictive accuracy and runs sequential Kelly betting strategy across bootstrap simulations (lets you define the bootstrap method parameters)
    - results are printed on standard output
    
2. evaluate_parallel_betting.py
    - does the same as 2. with the exception that the Kelly betting strategy treats windows of 10 fights as simultaneous fights and allocates wealth accordingly
